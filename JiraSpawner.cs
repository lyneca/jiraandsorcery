﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.ModelBinding;
using Atlassian.Jira;
using ExtensionMethods;
using ThunderRoad;
using UnityEngine;

namespace JiraAndSorcery {
    public class JiraSpawnerModule : ItemModule {
        public override void OnItemLoaded(Item item) {
            base.OnItemLoaded(item);
            item.gameObject.AddComponent<JiraSpawner>();
        }
    }

    public class JiraSpawner : MonoBehaviour {
        public Item item;
        private bool gripped;
        private bool loaded;
        private Dictionary<string, List<Ticket>> tickets;

        private Dictionary<string, int> statusOrder = new Dictionary<string, int>() {
            { "Backlog", 0 }, { "Selected for Development", 1 }, { "In Progress", 2 }, { "Done", 3 }, { "Won't Fix", 3 }
        };
        public void Start() {
            tickets = new Dictionary<string, List<Ticket>>();
            item = GetComponent<Item>();
            item.OnGrabEvent += (handle, hand) => gripped = true;
            item.mainCollisionHandler.OnCollisionStartEvent += instance => {
                if (gripped && !loaded) {
                    LevelModuleJira.LoadIssues(this);
                    loaded = true;
                }
            };
        }

        public void Init(Ticket ticket) {
            if (!tickets.ContainsKey(ticket.issue.Status.Name))
                tickets[ticket.issue.Status.Name] = new List<Ticket>();
            tickets[ticket.issue.Status.Name]?.Add(ticket);
        }

        public void Slice(Ticket ticket) {
            var status = ticket.issue.Status.Name;
            tickets[status].Remove(ticket);
            if (tickets[status].Count == 0) {
                tickets.Remove(status);
            }
        }
        public Vector3 GetPos(Ticket ticket) {
            Vector3 rightDir = Vector3.Cross(Player.currentCreature.GetHead().transform.position - transform.position,
                Vector3.up).normalized;
            float max = (tickets.Count == 0) ? 1 : tickets.Max(s => s.Value.Count);
            float x = statusOrder[ticket.issue.Status.Name] - (statusOrder.Max(kvp => kvp.Value) + 1) / 2f * 0.6f;
            float y = max * 0.4f + 0.3f - tickets[ticket.issue.Status.Name].IndexOf(ticket) * 0.4f;
            return transform.position + Vector3.up * y + x * rightDir;
        }

        public Vector3 GetRot(Ticket ticket) {
            return Player.currentCreature.GetHead().transform.position - ticket.transform.position;
        }
    }
}
