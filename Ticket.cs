﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlassian.Jira;
using ExtensionMethods;
using ThunderRoad;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace JiraAndSorcery {
    public class Ticket : MonoBehaviour {
        public Material mat;
        public Texture2D texture;
        public Issue issue;
        public JiraSpawner spawner;
        private bool init;
        private bool sliced;

        public static void New(Issue issue, JiraSpawner spawner) {
            Texture2D texture = LevelModuleJira.GetTexture();

            Catalog.GetData<ItemData>("JiraTicket").SpawnAsync(
                ticket => { ticket.gameObject.AddComponent<Ticket>().Init(issue, texture, spawner); },
                spawner.transform.position + Vector3.up * 2 + Utils.RandomVector(-2, 2),
                Quaternion.LookRotation(Utils.RandomVector(salt: 1)));
        }

        public Rigidbody rb;

        public void Init(Issue issue, Texture2D texture, JiraSpawner spawner, bool sliced = false) {
            if (init) return;
            init = true;
            this.spawner = spawner;
            this.issue = issue;
            this.texture = texture;
            this.sliced = sliced;
            mat = (transform.Find("mesh")?.GetComponent<MeshRenderer>() ?? GetComponentInChildren<MeshRenderer>())
                .material;
            mat.SetTexture("BaseMap", texture);
            if (!sliced) {
                spawner.Init(this);
            }
        }

        public void Slice() {
            spawner.Slice(this);

            issue.WorkflowTransitionAsync(LevelModuleJira.configClosedStatusName);
            //issue.SaveChanges();
            sliced = true;
        }

        private Utils.PIDRigidbodyHelper pid;

        public void Start() {
            rb = GetComponent<Rigidbody>();
            if (GetComponent<Item>() is Item item) {
                item.disallowDespawn = true;
                item.mainCollisionHandler.SetPhysicModifier(this, 3, 0);
            }

            pid = new Utils.PIDRigidbodyHelper(rb, 7, 3);
        }

        public void FixedUpdate() {
            if (GetComponent<Item>()?.mainHandler == null && !sliced) {
                pid.UpdateVelocity(spawner.GetPos(this));
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(spawner.GetRot(this)), Time.deltaTime * 10);
            }
        }
    }
}
