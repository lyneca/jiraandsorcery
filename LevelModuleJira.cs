﻿using System;
using System.Collections;
using System.Linq;
using System.Net.Mime;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using ThunderRoad;
using Atlassian.Jira;
using Atlassian.Jira.OAuth;
using ExtensionMethods;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace JiraAndSorcery {
    public class LevelModuleJira : LevelModule {
        public static int texSize = 1000;
        public string consumerKey;
        public string jiraUrl;
        
        public string accessToken;
        public string tokenSecret;
        public string consumerSecret;
        public string closedStatusName;

        public static RenderTexture renderTexture;
        public string issuePrefabAddress;
        public static GameObject issueCanvas;
        public static Jira jira;
        public static bool loaded;
        public static IssueStatus closedStatus;
        public static string configClosedStatusName;

        public override IEnumerator OnLoadCoroutine() {
            jira = Jira.CreateOAuthRestClient(jiraUrl, consumerKey,
                consumerSecret,
                accessToken, tokenSecret);
            jira.Statuses.GetStatusAsync(closedStatusName).Then(status => closedStatus = status);
            configClosedStatusName = closedStatusName;
            renderTexture = new RenderTexture(5 * texSize, 3 * texSize, 24);
            renderTexture.Create();
            Addressables.InstantiateAsync("Lyneca.JiraAndSorcery.TicketCanvas", Vector3.up * -100, Quaternion.identity).Task
                .Then(canvas => {
                    issueCanvas = canvas;
                    SetRenderTexture(renderTexture);
                });
            EventManager.onPossess += (creature, time) => {
                creature.handLeft.OnGrabEvent += (side, handle, position, orientation, eventTime)
                    => handle.item.gameObject.GetOrAddComponent<Slicer>();
                creature.handRight.OnGrabEvent += (side, handle, position, orientation, eventTime)
                    => handle.item.gameObject.GetOrAddComponent<Slicer>();
            };
            return base.OnLoadCoroutine();
        }

        public override void Update() {
            base.Update();
        }

        public static void LoadIssues(JiraSpawner spawner) {
            Task.Run(() => {
                foreach (var issue in jira.Issues.Queryable.Where(issue => issue.Project == "SORT")) {
                    GameManager.local.RunNextFrame(() => {
                        SetCanvas(issue);
                        Ticket.New(issue, spawner);
                    });
                }
            });
        }

        public static void SetCanvas(Issue issue) {
            SetField("Group/TitleAndStatus/Title", issue.Key.Value);
            SetField("Group/TitleAndStatus/Status", issue.Status.Name);
            SetField("Group/Bottom/Description", issue.Description);
        }

        public static void SetField(string field, string value) {
            Debug.Log($"Setting {field} to {value}");
            issueCanvas.transform.Find(field).GetComponent<Text>().text = value;
        }

        public static void SetRenderTexture(RenderTexture texture) {
            issueCanvas.transform.Find("RenderingCamera").GetComponent<Camera>().targetTexture = texture;
        }

        public static Texture2D GetTexture() {
            Debug.Log("Taking pic");
            issueCanvas.transform.Find("RenderingCamera").GetComponent<Camera>().Render();
            Texture2D output = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.RGBA32, false);
            output.Apply(false);
            Graphics.CopyTexture(renderTexture, output);
            return output;
        }
    }
}
