# Jira and Sorcery

A Blade and Sorcery mod that allows you to trim and refine your Jira backlog using a medieval greatsword.

## Setup

1. Buy and install Blade and Sorcery - [Steam](https://store.steampowered.com/app/629730/Blade_and_Sorcery/) / [Oculus](https://www.oculus.com/experiences/rift/2472374902778951/)
2. Copy the Mod/ folder to your mods folder and rename it, e.g.
   ```
   Steam\steamapps\common\Blade & Sorcery\BladeAndSorcery_Data\StreamingAssets\Mods\JiraAndSorcery
   ```
3. Follow the steps [here](https://bitbucket.org/farmas/atlassian.net-sdk/src/b3c4941f7482c1c4e00f229eef5e57ba1dd2df61/docs/how-to-connect-using-oauth.md) to generate the tokens and secrets you need.
4. Enter the secrets and configuration into `Level_Master.json` - you'll need:

   - `jiraUrl`
   - `consumerKey`
   - `accessToken`
   - `tokenSecret`
   - `consumerSecret`

5. You can customize what happens to tickets when they are destroyed by changing the `closedStatusName` field.

---

Is all that effort worth it? Was it even worth making this project?

No.
