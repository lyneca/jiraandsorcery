﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtensionMethods;
using ThunderRoad;
using UnityEngine;

namespace JiraAndSorcery {
    public class Slicer : MonoBehaviour {
        private float lastSlice;
        private Item item;

        void Start() {
            item = GetComponent<Item>();
            item.OnDespawnEvent += time => { Destroy(this); };
            item.mainCollisionHandler.OnCollisionStartEvent += hit => {
                if (hit.damageStruct.damageType == DamageType.Slash) {
                    var target = hit.targetCollider?.attachedRigidbody?.GetComponent<Ticket>();
                    if (target && Time.time - lastSlice > 1) {
                        lastSlice = Time.time;
                        foreach (var obj in MeshCut.Cut(target.gameObject, hit.contactPoint,
                            hit.damageStruct.damager.transform.right, target.mat)) {
                            obj.GetOrAddComponent<Ticket>().Init(target.issue, target.texture, target.spawner, true);
                        }
                    }
                }
            };
        }
    }
}
